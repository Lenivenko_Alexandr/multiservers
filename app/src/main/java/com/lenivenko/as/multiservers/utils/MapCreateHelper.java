package com.lenivenko.as.multiservers.utils;


import com.lenivenko.as.multiservers.model.ServerModel;
import java.util.concurrent.ConcurrentMap;

public class MapCreateHelper {
    public MapCreateHelper(ConcurrentMap<Integer, ServerModel> portsInfoMap, int serversQuantity) {
        createDefaultMap(portsInfoMap, serversQuantity);
    }

    public void createDefaultMap(ConcurrentMap<Integer,ServerModel> map, int quantityOfPort){

        for(int i = 0; i<quantityOfPort; i++){
            map.put(i,new ServerModel(0, Constant.FIRST_PORT_NUMBER + i));
        }

    }
}
