package com.lenivenko.as.multiservers.interfaces;

/**
 * serverPort
 * data         Input data
 * count        number of package which get from client tho current port
 */

public interface ServerCallBack {
    void onRequestFromClient(int serverPort,String data, int count);
}
