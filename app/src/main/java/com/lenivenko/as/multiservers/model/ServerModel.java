package com.lenivenko.as.multiservers.model;

/**
 * Class for one server
 */

public class ServerModel {

    private int mPort;
    private int amountOfDataPackages = 0;

    public ServerModel(int count, int port) {
        amountOfDataPackages = count;
        mPort = port;
    }

    public int getAmountOfDataPackages() {
        return amountOfDataPackages;
    }


    public int getPort() {
        return mPort;
    }

}
