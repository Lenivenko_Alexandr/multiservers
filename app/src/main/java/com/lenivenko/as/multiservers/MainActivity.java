package com.lenivenko.as.multiservers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.lenivenko.as.multiservers.adapter.ServersListIfoBaseAdapter;
import com.lenivenko.as.multiservers.interfaces.ServerCallBack;
import com.lenivenko.as.multiservers.model.ServerModel;
import com.lenivenko.as.multiservers.server.ThreadFactory;
import com.lenivenko.as.multiservers.utils.Constant;
import com.lenivenko.as.multiservers.utils.DeviceIpAddressDiscover;
import com.lenivenko.as.multiservers.utils.MapCreateHelper;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MainActivity extends AppCompatActivity implements ServerCallBack {



    private ConcurrentMap<Integer,ServerModel> portsInfoMap = new ConcurrentHashMap<>();
    private ServersListIfoBaseAdapter mAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView deviceIPTextView   = (TextView) findViewById(R.id.device_ip);
        ListView infoList           = (ListView) findViewById(R.id.info_list_view);

        deviceIPTextView.setText(new DeviceIpAddressDiscover().getDeviceIpAddress(this));

        new MapCreateHelper(portsInfoMap,Constant.SERVERS_QUANTITY) ;

        ThreadFactory threadFactory = new ThreadFactory(Constant.FIRST_PORT_NUMBER);
        threadFactory.setThreadsListener(this);
        threadFactory.runThreads();
        mAdapter = new ServersListIfoBaseAdapter(null);
        infoList.setAdapter(mAdapter);
        View viewEmptyList = findViewById(R.id.empty_view);
        infoList.setEmptyView(viewEmptyList);

    }

    @Override
    public void onRequestFromClient(int serverPort, String data, int count) {


            portsInfoMap.replace(serverPort-Constant.FIRST_PORT_NUMBER, new ServerModel(count,serverPort));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.addMap(portsInfoMap);
                }
            });

    }



}
