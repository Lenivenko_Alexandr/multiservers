package com.lenivenko.as.multiservers.utils;

import android.content.Context;
import android.util.Log;

import com.lenivenko.as.multiservers.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * DeviceIpAddressDiscover finds out Ip
 * but pay attention you have to add internet permissions
 *
 *       <uses-permission android:name="android.permission.INTERNET" />
         <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
         <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
         <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
 */

public class DeviceIpAddressDiscover {

    /**
     * Get ip address of the device
     */
    public String getDeviceIpAddress(Context context) {
        try {
            //Loop through all the network interface devices
            for (Enumeration<NetworkInterface> enumeration = NetworkInterface
                    .getNetworkInterfaces(); enumeration.hasMoreElements();) {
                NetworkInterface networkInterface = enumeration.nextElement();
                //Loop through all the ip addresses of the network interface devices
                for (Enumeration<InetAddress> enumerationIpAddr = networkInterface.getInetAddresses(); enumerationIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumerationIpAddr.nextElement();
                    //Filter out loopback address and other irrelevant ip addresses
                    if (!inetAddress.isLoopbackAddress() && inetAddress.getAddress().length == 4) {
                        //Print the device ip address in to the text view
                        return  context.getResources().getString(R.string.ip_adress) + inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("ERROR:", e.toString());
        }
        return context.getResources().getString(R.string.ip_adress) + "0.0.0.0";
    }
}
