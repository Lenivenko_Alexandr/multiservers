package com.lenivenko.as.multiservers.server;


import com.lenivenko.as.multiservers.interfaces.ServerCallBack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;



public class ServerSock extends Thread{

    private int mServerPort ;

    private ServerCallBack serverCallBack;

    private int mCount = 0;


    public ServerSock(int serverPort){
        mServerPort = serverPort;
    }

    @Override
    public void run() {
        try {
            //Create a server socket object and bind it to a port
            java.net.ServerSocket socServer = new java.net.ServerSocket(mServerPort);
            //Create server side client socket reference
            Socket socClient = null;
            //Infinite loop will listen for client requests to connect
            while (true) {

                //Accept the client connection and hand over communication to server side client socket
                socClient = socServer.accept();

                SocketServerReplyThread thread = new SocketServerReplyThread(socClient);
                thread.start();


            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private class SocketServerReplyThread extends Thread {

        private Socket hostThreadSocket;

        SocketServerReplyThread(Socket socket) {
            hostThreadSocket = socket;

        }

        @Override
        public void run() {
            String msgReply = "Hello from Android, you are #";

            try {
                //Get the data input stream comming from the client
                InputStream mInputStream = hostThreadSocket.getInputStream();
                //Get the output stream to the client
                PrintWriter out = new PrintWriter(
                        hostThreadSocket.getOutputStream(), true);
                //Write data to the data output stream
                out.println(msgReply);


                while (true){

                    //Buffer the data input stream
                    int bufferSize = 254;

                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(mInputStream),bufferSize);

                    //Read the contents of the data buffer
                    String request = null;
                    request = br.readLine();



                    if(!request.equals(null)){
                        serverCallBack.onRequestFromClient(mServerPort,request,++mCount);
                    }

                    //sleep
                    sleep(500);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                //Close the client connection
                try {
                    hostThreadSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }


    public void setServerCallBack(ServerCallBack serverCallBack) {
        this.serverCallBack = serverCallBack;
    }
}
