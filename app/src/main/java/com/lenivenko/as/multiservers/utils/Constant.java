package com.lenivenko.as.multiservers.utils;



public class Constant {
    public static final int FIRST_PORT_NUMBER = 56001;

    public static final int SERVERS_QUANTITY = 48;
}
