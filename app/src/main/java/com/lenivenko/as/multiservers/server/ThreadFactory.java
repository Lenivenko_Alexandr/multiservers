package com.lenivenko.as.multiservers.server;

import com.lenivenko.as.multiservers.interfaces.ServerCallBack;
import com.lenivenko.as.multiservers.utils.Constant;

import java.util.ArrayList;

/**
 * Class create N amount of thread
 * start port number should be done in constructor
 */

public class ThreadFactory {
    private int mStartPort;
    private ServerCallBack serverCallBack;
    ArrayList<ServerSock> mServersList = new ArrayList<>();

    public ThreadFactory(int startPort){
        mStartPort = startPort;
        createThreads();
    }

    private void createThreads(){

        int limit = mStartPort + Constant.SERVERS_QUANTITY;
        for(int port = mStartPort; port<limit; port++){
            mServersList.add(new ServerSock(port));

        }
    }

    //in case if serverCallBack hasn't been set
    //throw NullPointerException

    public void runThreads(){

        if(serverCallBack==null){
            throw new NullPointerException();
        }else {
            for(ServerSock serverSock : mServersList){
                serverSock.setServerCallBack(serverCallBack);
                serverSock.start();
            }
        }
    }

    public void setThreadsListener(ServerCallBack serverCallBack){
        this.serverCallBack = serverCallBack;
    }
}
