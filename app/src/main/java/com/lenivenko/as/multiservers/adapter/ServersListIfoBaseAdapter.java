package com.lenivenko.as.multiservers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lenivenko.as.multiservers.R;
import com.lenivenko.as.multiservers.model.ServerModel;
import com.lenivenko.as.multiservers.utils.Constant;

import java.util.concurrent.ConcurrentMap;

public class ServersListIfoBaseAdapter extends BaseAdapter {

    private ConcurrentMap<Integer,ServerModel> mServersInfoMap;

    public ServersListIfoBaseAdapter(ConcurrentMap<Integer,ServerModel> serversInfoMap){
        mServersInfoMap = serversInfoMap;
    }


    @Override
    public int getCount() {
        if(mServersInfoMap!=null){
            return Constant.SERVERS_QUANTITY; // instead of mServersInfoMap.size();  Used constant because it is look all item of collection
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mServersInfoMap.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if(view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_of_one_server, viewGroup, false);
        }

        ServerModel serverModel = ((ServerModel)getItem(position));

        TextView serverPortTextView = (TextView)view.findViewById(R.id.server_port);
        serverPortTextView.setText(serverModel.getPort()+"");

        TextView quantityOfPackagesTextView = (TextView)view.findViewById(R.id.quantity_of_packages);
        quantityOfPackagesTextView.setText(serverModel.getAmountOfDataPackages()+"");



        return view;
    }

    public void addMap(ConcurrentMap<Integer, ServerModel> portsInfoMap) {
        mServersInfoMap = portsInfoMap;
        notifyDataSetChanged();
    }
}
